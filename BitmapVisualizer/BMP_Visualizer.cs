﻿using Microsoft.VisualStudio.DebuggerVisualizers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

[assembly: DebuggerVisualizer(
typeof(BitmapVisualizer.BMP_Visualizer),
typeof(VisualizerObjectSource),
Target = typeof(Bitmap),
Description = "Bitmap Visualizer")]
namespace BitmapVisualizer
{
    /// <summary>
    /// Визуализатор для Bitmap.
    /// </summary>
    public class BMP_Visualizer : DialogDebuggerVisualizer
    {
        protected override void Show(IDialogVisualizerService windowService, IVisualizerObjectProvider objectProvider)
        {
            if (windowService == null)
                throw new ArgumentNullException("windowService");
            if (objectProvider == null)
                throw new ArgumentNullException("objectProvider");


            // TODO: получите объект, для которого нужно отобразить визуализатор.
            //       Выполните приведение результата objectProvider.GetObject() 
            //       к типу визуализируемого объекта.
            Bitmap BMP = null;
            try
            {
                BMP = (Bitmap)objectProvider.GetObject();
            }
            catch
            {
                BMP = null;
            }

            // TODO: отобразите свое представление объекта.
            //       Замените displayForm на свой объект Form или Control.
            if (BMP != null)
                using (Form displayForm = new Form())
                {
                    displayForm.StartPosition = FormStartPosition.CenterParent;
                    displayForm.Size = new Size(512, 512);
                    displayForm.BackgroundImageLayout = ImageLayout.Zoom;
                    displayForm.KeyPreview = true;
                    displayForm.KeyDown += new KeyEventHandler(delegate (Object o, KeyEventArgs e)
                    {
                        if (e.KeyCode == Keys.Escape)
                            displayForm.Close();
                    });

                    displayForm.Text = BMP.Size.ToString().Trim(new char[] { '{', '}' });
                    displayForm.BackgroundImage = BMP;
                    windowService.ShowDialog(displayForm);
                }
        }

        // TODO: добавьте следующее к своему коду тестирования для тестирования визуализатора:
        // 
        //    BMP_Visualizer.TestShowVisualizer(new SomeType());
        // 
        /// <summary>
        /// Тестирует визуализатор, разместив его вне отладчика.
        /// </summary>
        /// <param name="objectToVisualize">Объект для отображения в визуализаторе.</param>
        public static void TestShowVisualizer(object objectToVisualize)
        {
            VisualizerDevelopmentHost visualizerHost = new VisualizerDevelopmentHost(objectToVisualize, typeof(BMP_Visualizer));
            visualizerHost.ShowVisualizer();
        }
    }
}
