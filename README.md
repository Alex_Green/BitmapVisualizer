# BitmapVisualizer

## Description
Bitmap and Image Visualizer for Visual Studio 2015, 2017 (maybe lower too).

## How to use
Copy the "BitmapVisualizer.dll" to either of the following locations:

1. __VisualStudioInstallPath__\Common7\Packages\Debugger\Visualizers
2. My Documents\\__VisualStudio 20**__\Visualizers

## Link
https://gitlab.com/Alex_Green/BitmapVisualizer

https://msdn.microsoft.com/en-us/library/sb2yca43.aspx

## License:
**MIT**

## Author
Zelenskyi Alexandr (Зеленський Олександр) - ZAA93@yandex.ru