﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using BitmapVisualizer;

namespace BitmapVisualizer_Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Bitmap BMP = new Bitmap("Test.jpg");
            Image Img = Image.FromFile("Test.jpg");
            BMP_Visualizer.TestShowVisualizer(BMP);
            BMP_Visualizer.TestShowVisualizer(Img);
        }
    }
}